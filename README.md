# JOURNEY TRACKER
## Introduction
Journey Tracker is a website that allows the user to plot a route on a map of the world and track by distance they have moved alone it.

## Tech
The server for this site uses Node.js for the webserver, Vash for its templating engine and SQLite for its databases. Google Maps is used for displaying the world map. The client-side code is written in TypeScript.

## Settings.json
The Settings.json file is a JSON file that contains data that should not be included in the repo, e.g. the Google Maps API key, as well as various settings. Settings used by the server are:
- port: The port the server will listen to.
- version: Version of the website. Used to tell client to refresh if it doesn't have the latest version.
- api_key: Your API key for Google Maps.
- username: Username to login with.
- password: Password to login with.
- secret: Secret phrase used for encrypting cookies.
- subserver: If the server is being used as a subserver. (See notes.)

## Running
1. Install Node.js if it is not already installed.
2. Install the necessary Node.js modules using "npm install" in the root folder the project.
3. Create a settings.json file if one does not exist and use the "api_key" key to specify your Google Maps API key. Add "username" and "password" as well to be able to login. You can also add any other settings mentioned in the "Settings.json" section.
4. Build the client-side code using "npm run build-client".
5. Run using "npm start".

## Notes
- The animation currently used to indicate location was taken from the web and may be copyrighted. It should be replaced as soon as possible.
- The server is currently being used like a normal server when testing, but is a sub-server for production. Basically, a main server handles traffic, and routes it to this server. (The main server handles multiple sub-servers.) When this server is being tested, it handles its own traffic. To deal with this, the settings has the key "subserver" that is used to indicate if the server is acting as a subserver or not. If it is a subserver, then it doesn't listen to a port and exports its Express app object.
- The site was made for someone who would be the only person using the site, so it only features a single username and password to login. The site is still viewable in a read-only state when not logged in.
- Due to the site being designed for private usage, the username and password are currently stored in plain text. They can also not be changed remotely. (This will likely change if the site continues to be worked on.)

## Bugs
- Google Maps throws some errors when a route is first set.
- Google Maps throws some errors when the map is moved when a route is present, but only sometimes. (It seems to happen after refreshing the page soon after the page loaded, but not always.)