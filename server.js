const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const path = require('path');

//////////////////
///// CONSTS /////
//////////////////

// type of point for map
const LOCATION_TYPE = {
    START:0,
    END:1,
    WAYPOINT:2
};

// types of errors
const ERRORS = {
    SERVER_ERROR:1,
    MISC:2,
    NO_USERNAME:4,
    NO_PASSWORD:8,
    NO_NAME:16,
    ALREADY_EXISTS:32,
    INCORRECT:64,
    NOT_LOGGED_IN:128
};

const SID_LENGTH = 64;

////////////////////
///// SETTINGS /////
////////////////////

// default for settings
const defaults = {
    port:3000,
    subserver:false,
    version:"1",
    username:null,
    password:null,
    secret:"please set a secret phrase",
    obfuscated:false
};

// load settings
var settings = require('./custom_modules/Settings')('../settings.json', defaults);

/** @type {boolean} If login credentials have been provided */
let login_creds = (settings.username && settings.password);

if (!settings.username) {
    console.warn(`No username specified`);
}

////////////////////
///// DATABASE /////
////////////////////

const db = new sqlite3.Database(path.join(__dirname, 'data.db'), function(err) {
    if (err) {
        db.close();
        throw new Exception(`Error connecting to database: ${err}`);
    } else {
        // type is LOCATION_TYPE
        db.run(`CREATE TABLE IF NOT EXISTS markers(type INTEGER,lat FLOAT, lng FLOAT, idx INTEGER)`, function(err) {
            if (err) {
                throw new Exception(`Error creating database 'markers': ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS distances(distanceID INTEGER PRIMARY KEY, distance REAL, date INTEGER, deleted INTEGER DEFAULT 0)`, function(err) {
            if (err) {
                throw new Exception(`Error creating table 'distances': ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS sessions(sID integer)`, function(err) {
            if (err) {
                throw new Exception(`Error creating table 'sessions': ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS settings(key TEXT PRIMARY KEY, value TEXT)`, function(err) {
            if (err) {
                throw new Exception(`Error creating table 'settings': ${err}`);
            }
        });
    }
});

///////////////////
///// EXPRESS /////
///////////////////

const app = express();

// prepare for receiving json
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// make public folder assets accessible directly
app.use(express.static(path.join(__dirname, 'public')));

// enable Vash
app.set('view engine', 'vash');
app.set('views', path.join(__dirname, 'views'));

// start listening
if (!settings.subserver) {
    app.listen(settings.port, _=>console.log(`Connected to port ${settings.port}...`));
}

// if a subserver, expose app object for external use
if (settings.subserver) {
    exports.app = app;
}

////////////////////
///// SESSIONS /////
////////////////////

// set up cookies
const session = require('express-session');
const SQLiteStore = require('connect-sqlite3')(session);

app.use(session({
    secret:settings.secret,
    resave:true,
    saveUninitialized:false,
    store:new SQLiteStore()
}));

/////////////////////
///// FUNCTIONS /////
/////////////////////

/** 
 * Generate a random string of letters and numbers to use for session ID.
 * @param {number} length Length of session ID
 * @return {string} A session ID string
 */
function generateSID(length = SID_LENGTH) {
    let sID = '';

    for (let i = 0; i < length; i++) {
        let rnd = (Math.random() * 62) >>> 0;

        if (rnd < 10) {
            rnd += 48;
        } else if (rnd < 36) {
            rnd += 55;
        } else {
            rnd += 61;
        }

        sID += String.fromCharCode(rnd);
    }

    return sID;
}

/** 
 * Returns if the user is logged in.
 * @param {Request} req The request object
 * @return {boolean} If the user is logged in
 */
function isLoggedIn(req) {
    return new Promise((resolve, reject)=>{
        db.get(`SELECT count(*) FROM sessions WHERE sID=?`, req.session.sID, function(err, count) {
            if (err) {
                console.error(`Error getting session count: ${err}`);
                reject(err);
            } else {
                resolve(count["count(*)"] > 0);
            }
        });
    });
}

/**
 * Logs user in.
 * @param {Request} req The request object
 * @return {Promise<boolean>} Flags indicating why the login failed, or 0 if successful
 */
function logIn(req) {
    return new Promise(async (resolve, reject)=>{
        if (req.session.sID) {
            try {
                let loggedIn = await isLoggedIn(req);
                if (loggedIn) {
                    resolve(0);
                } else {
                    req.session.sID = null;
                    logIn(req)
                        .then(l=>resolve(l))
                        .catch(e=>reject(e));
                }
            } catch (err) {
                reject(err);
            }
        } else {
            // make sure necessary data is provided 
            let status = 0;
            let username = req.body.username,
                password = req.body.password;
            if (!username) {
                status |= ERRORS.NO_USERNAME;
            }
            if (!password) {
                status |= ERRORS.NO_PASSWORD;
            }

            // if error, send error code
            if (status) {
                resolve(status);
            } else {
                // check if logged in
                if (!login_creds || (username !== settings.username) || (password !== settings.password)) {
                    status = ERRORS.INCORRECT;
                    resolve(status);
                } else {
                    let sID = generateSID();
                    db.run(`INSERT INTO sessions VALUES (?)`, sID, function(err) {
                        if (err) {
                            console.error(`Error inserting session: ${sID}`);
                            reject(err);
                        } else {
                            req.session.sID = sID;
                            resolve(0);
                        }
                    });
                }
            }
        }
    });
}

/**
 * Gets total distance travelled.
 * @return {Promise<number>} Total distance travelled
 */
function getTotalProgress() {
    return new Promise(function(resolve, reject) {
        db.get('SELECT sum(distance) FROM distances WHERE deleted=0', function(err, distance) {
            if (err) {
                console.error(`Error getting sum of distance: ${err}`);
                reject(err);
            } else {
                let prog = distance["sum(distance)"] || 0;
                resolve(prog);
            }
        });
    });
}

/** 
 * Returns the setting(s) for a key or array of keys.
 * @param {string|string[]} keys Key(s) to get settings for
 * @return {Promise<string[]>} Requested settings
 */
function getSettings(keys) {
    return new Promise((resolve, reject)=>{
        if (!keys) {
            reject(`No key(s) provided for getting settings`);
        } else {
            let criteria = null;

            if (Array.isArray(keys)) {
                criteria = keys.map(k=>`key="${k}"`).join(" OR ");
            } else {
                criteria = `key="${keys}"`;
            }

            if (!criteria) {
                reject(`Error creating criteria for getting settings`);
            } else {
                db.all(`SELECT * FROM settings WHERE ${criteria}`, function(err, row) {
                    if (err) {
                        console.error(`Error getting setting '${keys}': ${err}`);
                        reject(err);
                    } else {
                        resolve(row);
                    }
                });
            }
        }
    });
}

/////////////////////
///// ENDPOINTS /////
/////////////////////

// main page
app.get('/', (req, res)=>{
    res.render('index', {api_key:settings.api_key});
});

app.route('/login')
.post(async function(req, res) {
    try {
        let status = await logIn(req);
        if (!status) {
            res.end();
        } else {
            res.status(400).end();
        }
    } catch (err) {
        res.status(500).end();
    }
});

app.route('/logout')
.post(async function(req, res) {
    let loggedIn = await isLoggedIn(req);

    if (loggedIn) {
        db.run(`DELETE FROM sessions WHERE rowid=(SELECT rowid FROM sessions WHERE sID=? LIMIT 1)`, req.session.sID, function(err) {
            if (err) {
                console.error(`Error deleting session: ${err}`);
                res.status(500).end();
            } else {
                req.session.sID = null;
                res.end();
            }
        });
    } else {
        res.end();
    }
});

///////////////
///// API /////
///////////////

app.route('/api/v1/data')
.get(async function(req, res) {
    try {
        let data = await Promise.all([
            isLoggedIn(req),
            getTotalProgress(),
            getSettings(['lockRoute', 'totalDistance'])
        ]);
        
        res.json({
            data:{
                loggedIn:data[0],
                totalProgress:data[1],                
                version:settings.version,
                settings:(data[2]||{})
            }
        });
    } catch (err) {
        console.error(`Error getting data: ${err}`);
        res.status(500).end();
    }
});

app.route('/api/v1/distances')
.get(async function(req, res) {    
    if (!req.params.action) {
        let loggedIn = await isLoggedIn(req);
        if (loggedIn) {
            db.all('SELECT * FROM distances WHERE deleted=0', function(err, distances) {
                if (err) {
                    console.error(`Error getting distances: ${err}`);
                    res.status(500).end();
                } else {
                    res.json({                        
                        distances:distances
                    });
                }
            });
        } else {
            res.status(403).end();
        }
    } else {
        res.status(405).end();
    }
})
.post(function(req, res) {
    if (!req.body) {
        res.status(400).end('No content body');
        return;
    }

    let distance = req.body.distance,
        date = req.body.date;

    if (!distance) {
        res.status(400).end('Distance not specified');
    } else {
        if (!date) {
            date = Date.now();
        }

        db.run('INSERT INTO distances(distance,date) VALUES(?,?)',
            [distance,date],
            async function(err) {
                if (err) {
                    console.error(`Error adding distance: ${err}`);
                    res.status(500).end();
                } else {
                    let distanceID = this.lastID;
                    try {
                        let progress = await getTotalProgress();
                        let data = {
                            distanceID:distanceID,
                            distance:distance,
                            date:date,
                            totalProgress:progress
                        };
                        res.json({data:data});
                    } catch (err) {
                        console.error(`Error getting sum of distance: ${err}`);
                        res.status(500).end();
                    }
                }
            });
    }
});

app.route('/api/v1/distances/:id')
.delete(function(req, res) {
    let id = req.params.id;
    db.run(`UPDATE distances SET deleted=1 WHERE distanceID=?`,id,async function(err){
        if (err) {
            console.error(`Error deleting distance: ${err}`);
            res.status(500).end();
        } else {
            try {
                let progress = await getTotalProgress();
                res.json({totalProgress:progress});
            } catch (err) {
                console.error(`Error getting total distance: ${err}`);
                res.status(500).end();
            }
        }
    });
});

app.route('/api/v1/map')
.get(function(req, res) {
    db.all(`SELECT * FROM markers`, function(err, markers) {
        if (err) {
            console.error(`Error getting markers: ${err}`);
            res.status(500).end();
        } else {
            let start = null,
                end = null,
                waypoints = [ ];

            markers.forEach(m=>{
                if (m.type === LOCATION_TYPE.START) {
                    start = { lat:m.lat, lng:m.lng };
                } else if (m.type === LOCATION_TYPE.END) {
                    end = { lat:m.lat, lng:m.lng };
                } else if (m.type === LOCATION_TYPE.WAYPOINT) {
                    waypoints.push({
                        location:{
                            lat:m.lat,
                            lng:m.lng
                        },
                        idx:m.idx
                    });
                }
            });

            if (start && end) {
                res.json({
                    route:{
                        start:start,
                        end:end,
                        waypoints:waypoints
                    }
                });
            } else {
                res.json({
                    route:null
                });
            }
        }
    });
});

app.route('/api/v1/settings')
.get(async function(req, res) {
    let loggedIn = await isLoggedIn(req);

    if (loggedIn) {
        db.all(`SELECT * FROM settings`, function(err, rows) {
            if (err) {
                console.error(`Error getting settings: ${err}`);
                res.status(500).end();
            } else {
                res.json({data:rows});
            }
        });
    } else {
        res.status(403).end();
    }
})
.put(async function(req, res) {
    let loggedIn = await isLoggedIn(req);

    if (!loggedIn) {
        res.status(403).end();
    } else if (!req.body) {
        res.status(400).end('No settings provided');
    } else {
        let settings = [ ],
            stmt = '';

        for (let i in req.body) {
            settings = settings.concat([i, req.body[i]]);            
            stmt += ' VALUES(?,?)';            
        }

        if (settings.length > 0) {
            db.run(`INSERT OR REPLACE INTO settings(key,value)${stmt}`, settings, function(err) {
                if (err) {
                    console.error(`Error updating settings: ${err}`);
                    res.status(500).end();
                } else {
                    res.end();
                }
            });
        } else {
            res.end();
        }
    }
});

app.route('/api/v1/route')
.put(function(req, res) {
    let route = req.body;
    if (route) {
        if (!route.start) {
            let err = `Error: No start point in route.`;
            console.error(err);
            res.status(400).end(err);
        } else if (!route.end) {
            let err = `Error: No end point in route.`;
            console.error(err);
            res.status(400).end(err);
        } else {
            db.run(`DELETE FROM markers`, function(err) {
                if (err) {
                    console.error(`Error deleting old markers: ${err}`);
                    res.status(500).end();
                } else {
                    // TODO: Handle an insert failing better.
                    db.parallelize(_=>{
                        db.run(`INSERT INTO markers(type,lat,lng) VALUES(?,?,?)`,
                            [LOCATION_TYPE.START,route.start.lat,route.start.lng],
                            function(err) {
                                if (err) {
                                    console.error(`Error inserting start point: ${err}`);
                                }
                            });
                        db.run(`INSERT INTO markers(type,lat,lng) VALUES(?,?,?)`,
                            [LOCATION_TYPE.END,route.end.lat,route.end.lng],
                            function(err) {
                                if (err) {
                                    console.error(`Error inserting end point: ${err}`);
                                }
                            });

                        if (route.waypoints) {
                            let complete = function(err) {
                                if (err) {
                                    console.error(`Error inserting waypoint ${i} point: ${err}`);
                                }
                            };

                            for (let i = 0; i < route.waypoints.length; i++) {
                                db.run(`INSERT INTO markers(type,lat,lng,idx) VALUES(?,?,?,?)`,
                                    [LOCATION_TYPE.WAYPOINT,route.waypoints[i].location.lat,route.waypoints[i].location.lng,i],
                                    complete
                                );
                            }
                        }

                        if (typeof route.totalDistance !== "undefined") {
                            db.run(`INSERT OR REPLACE INTO settings(key,value) VALUES('totalDistance',?)`,route.totalDistance,function(err) {
                                if (err) {
                                    console.error(`Error setting totalDistance: ${err}`);
                                }
                            });
                        }
                    });

                    res.end();
                }
            });
        }
    } else {
        console.error(`Error: No route provided in data`);
        res.status(400).end('No route provided');
    }  
})
.delete(function(req, res) {
    db.run('DELETE FROM markers', function(err) {
        if (err) {
            console.error(`Error resetting route: ${err}`);
            res.status(500).end();
        } else {
            res.end();
        }
    });
});