const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    output:{
        path: __dirname + '/public',
        filename: 'index.js'
    },
    resolve:{
        extensions:['.ts']
    },
    module:{
        rules:[
            {test:/\.ts/, loader: 'ts-loader'}
        ]
    }
};