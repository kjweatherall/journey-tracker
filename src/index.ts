import Map from './map';

///////////////
///// MAP /////
///////////////

let map = new Map();

(window as any).initMap = function() {
    map.init()
    .then(function() {
        hideLoading();
        map.showProgress();
    })
    .catch(function() {
        hideLoading();
    });
}

///////////////////
///// LOADING /////
///////////////////

// shows loading screen
function showLoading() {
    (document.querySelector('#loading') as HTMLDivElement).style.display = "block";
}

// hides loading screen
function hideLoading() {
    (document.querySelector('#loading') as HTMLDivElement).style.display = "none";
}

////////////////////
///// DISTANCE /////
////////////////////

// sends new distance to server
function sendDistance() {
    let distance: number = +(document.querySelector('#distance') as HTMLInputElement).value,
        date: string = (document.querySelector('#date') as HTMLInputElement).value;

    let errors: string[] = [ ];

    // check user input
    if (isNaN(distance)) {
        errors.push('Distance is not a number');
    } else if (distance <= 0) {
        errors.push('Distance must be greater than 0');
    }

    showErrors('errorDistance', errors);

    // don't send to server if there are errors
    if (errors.length === 0) {
        // send to server
        fetch('/api/v1/distances',{method:"post",headers:{"Content-Type":"application/json"},body:JSON.stringify({distance:distance,date:date})})
        .then(res=>{
            if (res.status === 200) {
                res.json()
                .then(json=>{
                    let dist = json.data;
                    if (dist.totalProgress && !isNaN(dist.totalProgress)) {                        
                        map.totalProgress = dist.totalProgress;
                    }
                    map.showProgress();
                    displayDistance(dist.distance, dist.distanceID, dist.date);
                    updateUI();
                })
                .catch(err=>{
                    console.error(`Error parsing data: ${err}`);
                });
            } else {
                showErrors('errorDistance', res.statusText, res.status);
            }
        })
        .catch(err=>{
            showErrors('errorDistance', err);
        });
    }
}

// removes a distance
function removeDistance(id: number) {
    return new Promise(function(resolve, reject) {
        // send request to server
        fetch(`/api/v1/distances/${id}`,{method:"delete"})
        .then(res=>{
            if (res.status === 200) {
                res.json()
                .then(json=>{
                    map.totalProgress = json.totalProgress;
                    map.showProgress();
                    resolve();
                })
                .catch(err=>{
                    alert(`Error parsing new total distance: ${err}`);
                })
            } else if (res.status === 500) {
                alert(`Server error`);
            }
        })
        .catch(err=>{
            alert(`Error removing distance: ${err}`);
        });
    });    
}

/////////////////
///// LOGIN /////
/////////////////

// hides login dialog
function hideLogin() {
    (document.querySelector('#login') as HTMLDivElement).style.display = "none";
}

// toggles visibility of login dialog
function toggleLogin() {
    let login: HTMLDivElement = document.querySelector('#login');
    login.style.display = (login.style.display === "none" ? "block" : "none");
}

// makes changes relative to being logged in
function switchToLoggedIn() {
    (document.querySelector('#loggedIn') as HTMLDivElement).style.display = "block";
    (document.querySelector('#loggedOut') as HTMLDivElement).style.display = "none";

    map.login();
}

// makes changes relative to being logged out
function switchToLoggedOut() {
    (document.querySelector('#loggedIn') as HTMLDivElement).style.display = "none";
    (document.querySelector('#loggedOut') as HTMLDivElement).style.display = "block";

    map.logout();
}

// logs in
function login() {
    let elemUsername: HTMLInputElement = document.querySelector('#username'),
        elemPassword: HTMLInputElement = document.querySelector('#password'),
        errors: HTMLDivElement = document.querySelector('#errorLogin');

    let username = elemUsername.value,
        password = elemPassword.value;
        
    errors.innerHTML = "";

    // check if username and password given
    if (!username || !password) {
        let err = '';
        if (!username) {
            err = '- No username specified';
        }
        if (!password) {
            if (err.length !== 0) {
                err += '\n';
            }
            err += '- No password specified';
        }
        alert(`Error logging in: ${err}`);
    } else {
        // disable button until logged in
        let btnLogin = document.querySelector('#buttonLogin');
        btnLogin.setAttribute('disabled', 'disabled');

        // send login request to server
        fetch('/login',
            {method:"post",headers:{"Content-Type":"application/json"},
            body:JSON.stringify({username:username,password:password})})
        .then(res=>{
            if (res.status === 200) {
                switchToLoggedIn();
                hideLogin();

                elemUsername.value = '';
                elemPassword.value = '';
            } else if (res.status === 400) {
                alert(`Incorrect login details`);
            } else if (res.status === 500) {
                alert(`Server error`);
            } else {
                alert(`Unknown error`);
            }

            btnLogin.removeAttribute('disabled');
        })
        .catch(err=>{
            alert(`Error logging in: ${err}`);
            btnLogin.removeAttribute('disabled');
        });
    }

    event.preventDefault();
}

// logs out
function logout() {
    // diable button until logged out
    let btnLogout: HTMLButtonElement = document.querySelector('#buttonLogout');
    
    btnLogout.setAttribute('disabled', 'disabled');

    // send logout request
    fetch('/logout', {method:"post",headers:{"Content-Type":"application/json"}})
    .then(res=>{
        if (res.status === 200) {
            switchToLoggedOut();
        } else {
            alert(`Error logging out`);
        }

        btnLogout.removeAttribute('disabled');
    })
    .catch(err=>{
        alert(`Error logging out: ${err}`);
        btnLogout.removeAttribute('disabled');
    });
}

/////////////////
///// ADMIN /////
/////////////////

// types of tab
const TABS = {
    DISTANCES: 0,
    ROUTE: 1
};

// show specific tab in admin console
function showTab(tabID: number) {
    // show/hide relative panels
    (document.querySelector('#distancesPanel') as HTMLDivElement).style.display = (tabID === TABS.DISTANCES ? 'block' : 'none');
    (document.querySelector('#routePanel') as HTMLDivElement).style.display = (tabID === TABS.ROUTE ? 'block' : 'none');

    // highlight/dehighlight relative tabs
    document.querySelector('#tabDistances').className = (tabID === TABS.DISTANCES ? 'nav-link active' : 'nav-link');
    document.querySelector('#tabRoute').className = (tabID === TABS.ROUTE ? 'nav-link active' : 'nav-link');
}

// adds distance to admin UI
function displayDistance(distance: number, distID: number, date: string, distElem: HTMLDivElement = null) {
    if (!distElem) {
        distElem = document.querySelector('#distances');
    }

    if (!distElem) {
        alert(`Error! Cannot find element for distances. Please contact developer.`);
        return;
    }

    // main element
    let elem = document.createElement('div');

    // add remove button
    let btn: HTMLButtonElement = document.createElement('button');
    btn.type = 'button';                        
    btn.textContent = 'X';
    btn.onclick = function() {
        removeDistance(distID)
        .then(function() {
            btn.parentElement.remove();
            updateUI();
        })
        .catch(err=>{
            alert(err);
        });
    };
    elem.appendChild(btn);

    // add distance text
    let txt = document.createTextNode(`${date}: ${distance.toFixed(2)}km`);
    elem.appendChild(txt);

    // add element to UI
    distElem.appendChild(elem);
}

// bring up admin UI
function showAdmin() {
    showTab(TABS.DISTANCES);

    let distElem: HTMLDivElement = document.querySelector(`#distances`);
    distElem.innerHTML = '';

    showPanel('adminPanel');

    // send request to server
    fetch('/api/v1/distances')
    .then(res=>{
        if (res.status === 200) {
            res.json()
            .then(json=>{
                distElem.innerHTML = '';

                // display distances
                if (json.distances) {
                    for (let i of json.distances) {
                        displayDistance(i.distance, i.distanceID, i.date, distElem);
                    }
                }
            })
            .catch(err=>{
                alert(`Error parsing distances data: ${err}`);
            });
        } else {
            alert(`Error getting distances (${res.status})`);
        }
    })
    .catch(err=>{
        console.error(`Error getting distances: ${err}`);
    });
}

//////////////////
///// PANELS /////
//////////////////

// which tab of admin panel is showing
var panel: HTMLDivElement = null;

// shows specified panel
function showPanel(id: string) {
    if (panel) {
        hidePanel();
    }

    panel = document.querySelector(`#${id}`);
    if (!panel) {
        alert(`Cannot find panel '${id}'. Please contact developer.`);
        return;
    }

    (document.querySelector('#panelBg') as HTMLDivElement).style.display = 'grid';
    panel.style.display = 'block';
}

// hides specified panel
function hidePanel() {
    if (panel) {
        panel.style.display = 'none';
        panel = null;

        (document.querySelector('#panelBg') as HTMLDivElement).style.display = 'none';
    }
}

////////////////////
///// UPDATING /////
////////////////////

// shows "update" banner
function showBanner() {
    (document.querySelector('#bannerRefresh') as HTMLDivElement).style.display = 'block';
    (document.querySelector('#controls') as HTMLDivElement).style.paddingTop = '30px';
}

// hides "update" banner
function hideBanner() {
    (document.querySelector('#bannerRefresh') as HTMLDivElement).style.display = 'none';
    (document.querySelector('#controls') as HTMLDivElement).style.paddingTop = '0';

    ignoreUpdate = true;

    event.stopPropagation();
}

// refreshes page
function refresh() {
    location.reload();
}

// if data has been fetched
var _data = false;
// version of site loaded
var version = -1;
// if user has dismissed "update available" notification
var ignoreUpdate = false;
// if the user is logged in
var loggedIn = false;

// get necessary data
fetch('/api/v1/data')
.then(res=>{
    if (res.status === 200) {
        res.json()
        .then(json=>{
            _data = true;
            processData(json.data);
        })
        .catch(err=>{
            console.error(`Error parsing data: ${err}`);
            alert(`Error with data`);
        });
    } else {
        alert(`Error getting data (${res.status})`);
    }
})
.catch(err=>{
    alert(`Error fetching data: ${err}`);
});

// process data received from server
function processData(data: any) {
    // handle change in logged in status
    if (loggedIn !== data.loggedIn) {
        loggedIn = data.loggedIn;
        if (loggedIn) {
            switchToLoggedIn();
        } else {
            switchToLoggedOut();
        }
    }

    console.log(version + "; " + data.version + "; " + ignoreUpdate);

    // handle change in version
    if (version === -1) {
        version = data.version;
    } else if (data.version !== version && !ignoreUpdate) {
        showBanner();
    }

    // settings
    let settings: {[s:string]:any}[] = data.settings;
    if (settings) {
        let lr = settings.find(s=>s.key==="lockRoute");
        let td = settings.find(s=>s.key==="totalDistance");

        if (lr) {
            let elem: HTMLInputElement = document.querySelector('#lockRoute');
            if (elem) {
                elem.checked = (lr.value === "true");
            }
        }

        if (td) {
            map.totalDistance = td.value>>>0;
        }
    }

    map.totalProgress = data.totalProgress;

    // update ui
    triggerUI();
    updateUI();
    map.showProgress();
}

// fetch latest data from server
function update() {
    fetch('/api/v1/data')
    .then(res=>{
        if (res.status === 200) {
            res.json()
            .then(json=>{
                processData(json.data);
            })
            .catch(err=>{
                alert(`Error parsing update data: ${err}`);
            });
        } else {
            alert(`Error updating (${res.status})`);
        }
    })
    .catch(err=>{
        console.error(`Error updating: ${err}`);
    });
}

/////////////////////
///// FUNCTIONS /////
/////////////////////

// add a leading 0 to a number if it's less than 10
function fixNum(num: number): string {
    return (num < 10) ? ('0' + num) : num.toString();
}

// shows errors using specified element
function showErrors(errDiv: string, errors: string | string[], errorCode: number = -1) {
    let errElem = document.querySelector(`#${errDiv}`);
    if (!errElem) {
        console.warn(`Cannot find error div for '${errDiv}'`);
        return;
    }

    errElem.innerHTML = '';

    if (!errors) {
        return;
    }
    
    if (Array.isArray(errors)) {
        for (let i of errors) {
            errElem.innerHTML += `<div>${i}</div>`;
        }
    } else {
        errElem.innerHTML += `<div>${errors}</div>`;
    }

    if (errorCode !== -1) {
        errElem.innerHTML += `<div>Error code: ${errorCode}</div>`;
    }
}

// display correct UI when page loaded
function triggerUI() {
    if (_loaded && _data) {
        (document.querySelector('#controls') as HTMLDivElement).style.display = "block";

        if (loggedIn) {
            switchToLoggedIn();
        } else {
            switchToLoggedOut();
        }
    }
}

// updates elements of the UI with the latest data
function updateUI () {
    let tp = document.querySelector('#totalProgress');
    let td = document.querySelector('#totalDistance');

    if (tp && td) {
        tp.innerHTML = map.totalProgress.toString();
        td.innerHTML = map.totalDistance.toString();
    }
}

function lockRoute() {
    let elem: HTMLInputElement = document.querySelector('#lockRoute');
    map.setLockRoute(elem);
}

// if page has loaded
var _loaded = false;

window.onload = function() {
    _loaded = true;

    // add event listeners
    document.querySelector('#buttonShowLogin').addEventListener('click', toggleLogin);
    document.querySelector('#formLogin').addEventListener('submit', login);
    document.querySelector('#buttonAdmin').addEventListener('click', showAdmin);
    document.querySelector('#buttonLogout').addEventListener('click', logout);
    document.querySelector('#buttonHidePanel').addEventListener('click', hidePanel);
    document.querySelector('#buttonSendDistance').addEventListener('click', sendDistance);
    document.querySelector('#bannerRefresh').addEventListener('click', refresh);
    document.querySelector('#btnRefreshClose').addEventListener('click', hideBanner);
    document.querySelector('#lockRoute').addEventListener('click', lockRoute);

    document.querySelector('#formRoute').addEventListener('submit', function() {
        let from = (document.querySelector('#from') as HTMLInputElement).value,
            to = (document.querySelector('#to') as HTMLInputElement).value;

        map.setRoute(from, to)
        .then(function() {
            hidePanel();
            updateUI();
        })
        .catch(function(err) {
            alert(err);
        });

        event.preventDefault();
    });
    document.querySelector('#tabDistances').addEventListener('click', function() {
        showTab(TABS.DISTANCES);
    });
    document.querySelector('#tabRoute').addEventListener('click', function() {
        showTab(TABS.ROUTE);
    });

    // match height of map to screen height
    let canvas: HTMLDivElement = document.querySelector('#map_canvas');
    canvas.style.height = window.innerHeight + "px";
    canvas.style.display = "block";

    // display today's date by default for distance menu
    let now = new Date();
    (document.querySelector('#date') as HTMLInputElement).value=`${now.getFullYear()}-${fixNum(now.getMonth() + 1)}-${fixNum(now.getDate())}`;

    // set UI based on if logged in
    triggerUI();

    // get updates
    // setInterval(update, 6000);
};

// update size of map when window size changes
window.onresize = function() {
    let canvas: HTMLDivElement = document.querySelector('#map_canvas');
    canvas.style.height = window.innerHeight + "px";
    canvas.style.display = "block";
};