import * as Settings from '../settings.json';

interface Location {
    lat: number,
    lng: number
}

interface Waypoint {
    idx: number,
    location: Location
}

interface Route {
    start: Location,
    end: Location,
    waypoints: Waypoint[]
}

export default class {    
    map: google.maps.Map = null;
    service: google.maps.DirectionsService = null;    
    display: google.maps.DirectionsRenderer = null;    
    
    totalProgress = 0; // total distance travelled    
    totalDistance = 0; // total distance to travel    
    isRoute = false;   // if there is a route and it has been loaded
    marker: google.maps.Marker = null; // progress indicator

    // initialise the map
    init(): Promise<void> {
        return new Promise((resolve, reject)=>{
            this.service = new google.maps.DirectionsService();
            this.display = new google.maps.DirectionsRenderer({
                // suppressMarkers:loggedIn
            });

            // default location
            let dunedin = {
                lat:-45.87417442544962,
                lng:170.50360679626468
            };

            // temp data for testing purposes
            let portChalmers = {
                lat:-45.815793,
                lng:170.621359
            };

            // set up map
            this.map = new google.maps.Map(document.querySelector('#map_canvas'), {
                zoom:10,
                center:dunedin,
                disableDefaultUI:true,
                zoomControl:true
            });

            this.display.setMap(this.map);

            google.maps.event.addListener(this.display, 'directions_changed', ()=>{
                this.updateRoute();
            });

            // get map data from server
            fetch('/api/v1/map')
            .then(res=>{
                if (res.status === 200) {
                    res.json()
                    .then(json=>{
                        let route: Route = json.route;
                        if (route) {
                            let wps = route.waypoints.sort((a,b)=>a.idx-b.idx).map(wp=>({location:new google.maps.LatLng(wp.location.lat, wp.location.lng),stopover:false}));

                            this.service.route({
                                origin:route.start,
                                destination:route.end,
                                waypoints:wps,
                                travelMode: google.maps.TravelMode.DRIVING
                            }, (response: google.maps.DirectionsResult, status: any)=>{
                                if (status === 'OK') {
                                    this.display.setDirections(response);
                                    this.isRoute = true;
                                    this.doZoom();

                                    resolve();
                                } else {
                                    console.error(`Directions request failed due to ${status}.`);
                                    reject();
                                }
                            });
                        } else {
                            resolve();
                        }
                    })
                    .catch(err=>{
                        alert(`Error parsing map data: ${err}`);
                    })
                } else if (res.status === 500) {
                    alert(`Server error`);
                } else {
                    alert(`Error getting map data (${res.status})`);
                }
            })
            .catch(err=>{
                alert(`Error getting map details: ${err}`);
            });
        });
    }

    // called when user logs in
    login() {
        fetch('/api/v1/settings')
        .then(res=>{
            if (res.status === 200) {
                res.json()
                .then(json=>{
                    let data: {[s:string]:any}[] = json.data;
                    if (data) {
                        let setting = data.find(d=>d.key==="lockRoute");                    
                        console.log(setting);
                        if (setting) {
                            let val = (setting.value === "false");
                            this.setRouteEditable(val);
                        } else {
                            this.setRouteEditable(true);
                        }
                    }
                })
                .catch(err=>{
                    alert(`Error parsing settings: ${err}`);
                });
            } else if (res.status === 500) {
                alert(`Server error`);
            } else {
                alert(`Error getting settings (${res.status})`);
            }
        })
        .catch(err=>{
            alert(`Error getting settings: ${err}`);
        });
    }

    // called when user logs out
    logout() {
        this.setRouteEditable(false);
    }

    // calculates and sets location for progress indicator
    showProgress() {
        if (!this.display) {
            return;
        }
    
        const dirs = this.display.getDirections();
    
        if (!dirs || !dirs.routes) {
            return;
        }
    
        const route = dirs.routes[0];
        const path = route.overview_path;
    
        if (this.totalProgress <= 0 || !this.isRoute) {
            if (this.isRoute) {
                this.setMarker(path[0], path[1]);
            }
            return;
        }
    
        const calculator = google.maps.geometry.spherical;
    
        // progress stored as km; convert to m
        let progress = this.totalProgress * 1000;
        let dist = 0;
        let leg = route.legs[0];
        let steps = leg.steps;
        let prev = steps[0].path[0];
    
        // calculate the location to play the indicator by iterating through points along the route
        // until a point is equal to or further than the progress distance
        for (let i = 0; i < steps.length; i++) {
            for (let j = 0; j < steps[i].path.length; j++) {
                let p = steps[i].path[j];
    
                // calculate distance between point and previous point and add to total
                dist += calculator.computeDistanceBetween(prev, p);
    
                if (dist >= progress) {
                    let next;
                    // if point isn't the final one on path
                    if (j < steps[i].path.length - 1) {
                        next = steps[i].path[j + 1];
                    // if step is not final one
                    } else if (i < steps.length) {
                        next = steps[i + 1].path[0];
                    // point is final point on route
                    } else {
                        next = p;
                        p = prev;
                    }
    
                    this.setMarker(p, next);
                    return;
                }
    
                prev = p;
            }
        }
    
        // if total progress exceeds the end of the route, place marker at end of route
        let lastStep = steps[steps.length - 1];
        this.setMarker(lastStep.path[lastStep.path.length - 2], lastStep.path[lastStep.path.length - 1], true);
    }

    // called when changed if route is editable
    private setRouteEditable(val: boolean) {
        if (this.display) {
            this.display.setOptions({
                suppressMarkers:!val,
                draggable:val
            });
        }
    }

    // sets if route should be locked
    setLockRoute(chkbox: HTMLInputElement) {
        if (this.display) {
            let val = chkbox.checked;

            chkbox.setAttribute('disabled','disabled');

            fetch('/api/v1/settings', {method:"put",headers:{"Content-Type":"application/json"},body:JSON.stringify({lockRoute:val.toString()})})
            .then(res=>{
                chkbox.removeAttribute('disabled');

                if (res.status === 200) {
                    this.setRouteEditable(!val);
                } else if (res.status === 500) {
                    alert(`Server error`);
                } else {
                    alert(`Error locking route (${res.status})`);
                }
            })
            .catch(err=>{
                alert(`Error updating settings: ${err}`);
            });
        }
    }

    // fit route to screen
    // https://stackoverflow.com/questions/12678786/google-maps-v3-zoom-to-fit-all-markerspath-function
    private doZoom() {
        let bounds = new google.maps.LatLngBounds();
        
        const dirs = this.display.getDirections();
        const route = dirs.routes[0];
        const path = route.overview_path;

        path.forEach(latLng=>{
            bounds.extend(latLng);
        });

        this.map.fitBounds(bounds);
    }

    // updates data and sends data to server when route changed
    private updateRoute() {
        let dir = this.display.getDirections();
        let waypoints:{location:Location}[]  = [ ];
        let leg = dir.routes[0].legs[0];
        
        // get each waypoint along route
        leg.via_waypoints.forEach(wp=>{
            waypoints.push({
                location:{lat:wp.lat(),lng:wp.lng()}
            });
        });

        // calculate the new length of route
        this.totalDistance = 0;
        let route = dir.routes[0];
        route.legs.forEach(l=>this.totalDistance += l.distance.value);
        this.totalDistance /= 1000;

        let start = {
            lat:leg.start_location.lat(),
            lng:leg.start_location.lng()
        };

        let end = {
            lat:leg.end_location.lat(),
            lng:leg.end_location.lng()
        };

        // compile data and send to server
        let data = {
            start:start,
            end:end,
            waypoints:waypoints,
            totalDistance:this.totalDistance
        };

        fetch('/api/v1/route', {method:"PUT",headers:{"Content-Type":"application/json"}, body:JSON.stringify(data)})
        .then(res=>{
            if (res.status === 200) {
                this.isRoute = true;
            } else if (res.status === 500) {
                alert(`Server error`);
            } else {
                alert(`Error updating route (${res.status})`);
            }

            this.showProgress();
        })
        .catch(err=>{
            alert(`Error updating route: ${err}`);
        });
    }

    // sets location of indicator for progress along route
    // setToP2 specifies is p2 should be used as the location for the indicator
    private setMarker(p1: google.maps.LatLng, p2: google.maps.LatLng, setToP2: boolean = false) {
        // remove existing marker
        if (this.marker) {
            this.marker.setMap(null);
            this.marker = null;
        }

        let file = (p2.lng() > p1.lng()) ? "right" : "left";

        let data = {
            map:this.map,
            position:(setToP2 ? p2 : p1),
            icon:file+'.gif'
        };

        this.marker = new google.maps.Marker(data);
    }

    // sets the route
    resetRoute() {
        fetch('/api/v1/route', {method:"delete"})
        .then(res=>{
            if (res.status === 200) {
                this.isRoute = false;
                this.showProgress();
                this.display.setMap(null);
            } else if (res.status === 500) {
                alert(`Server error`);
            } else {
                alert(`Error resetting route (${res.status})`);
            }
        })
        .catch(err=>{
            alert(`Error fetching route rest: ${err}`);
        });
    }

    // called when a new route is to be set
    setRoute(from: string, to: string): Promise<void> {
        return new Promise((resolve, reject)=>{
            // document.querySelector('#btnRoute').setAttribute('disabled', 'disabled');

            // let from = (document.querySelector('#from') as HTMLInputElement).value;
            // let to = (document.querySelector('#to') as HTMLInputElement).value;

            fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${to}&key=${Settings.api_key}`)
            .then(data=>data.json())
            .then(jsonTo=>{
                fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${from}&key=${Settings.api_key}`)
                    .then(data=>data.json())
                    .then(jsonFrom=>{
                        console.log(jsonFrom);

                        let posFrom = {
                            lat:jsonFrom.results[0].geometry.location.lat,
                            lng:jsonFrom.results[0].geometry.location.lng
                        };
                        let posTo = {
                            lat:jsonTo.results[0].geometry.location.lat,
                            lng:jsonTo.results[0].geometry.location.lng
                        };

                        this.service.route({
                            origin:posFrom,
                            destination:posTo,                            
                            travelMode: google.maps.TravelMode.DRIVING
                        }, (response: google.maps.DirectionsResult, status: google.maps.DirectionsStatus)=>{
                            document.querySelector('#btnRoute').removeAttribute('disabled');                        

                            if (status === google.maps.DirectionsStatus.OK) {
                                document.querySelector('#errorRoute').innerHTML = "";
                                this.display.setMap(this.map);
                                this.display.setDirections(response);
                                // this.hidePanel();
                                resolve();
                            } else {
                                document.querySelector('#errorRoute').innerHTML = status.toString();
                                // this.showErrors('errorRoute', [
                                //     status === google.maps.DirectionsStatus.ZERO_RESULTS ? 'Location not found, multiple locations with no or no viable path' : status
                                // ]);

                                let error: string = (status === google.maps.DirectionsStatus.ZERO_RESULTS ? 'Location not found, multiple locations with no or no viable path' : status.toString());

                                reject(error);
                            }
                        });
                    });
            });
        });
    }
}